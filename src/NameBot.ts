import { KlasaClient, Client, KlasaClientOptions } from 'klasa';

export default class NameBot extends Client {
  constructor(options: KlasaClientOptions | undefined) {
    super(options);
  }
}

const nb = new NameBot({
  prefix: 'n.'
});

nb.login();